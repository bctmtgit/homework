list_of_words = []
enter_any_words = input("Please enter any words you want:")
for element in enter_any_words.split():
    if element.endswith('o') or element.endswith('O'):
        list_of_words.append(element)
length = len(list_of_words)
quantity = "You've entered {} word(s) ending with letter 'o' or 'O'."
print(quantity.format(length))
