import requests
url = 'https://dummyjson.com/users?limit=100'
response = requests.get(url)
webpage_data = response.json()
users = webpage_data['users']
total_age = 0
quantity = 0
for person in users:
    if person['hair']['color'].lower() == 'brown':
        quantity += 1
        total_age += person['age']
average_age = total_age // quantity if quantity > 0 else None
print('The average age of men with brown hair is ' + str(average_age)) if average_age else print('No results found')
