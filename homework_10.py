﻿"""
simulator for a store cash register
code, shown below, takes from a customer information below about purchasing two products:
- name
- amount (integer)
- price per unit
receipt is being formed based on the data received
all prices and costs must be displayed in the next format: '0.00', amount - integers
the program is designed to use in Ukraine
"""
import textwrap
from datetime import datetime
from decimal import Decimal

# goods 1 section
item_1_title = textwrap.shorten(input('The name of the first product: ').ljust(20, '.'), width=20, placeholder='...')
item_1_quantity = Decimal(input('Enter the desired amount of the first product: '))
item_1_quantity_right_format = item_1_quantity.quantize(Decimal('1.00'))
item_1_zina = Decimal(input('Enter the price of the first product: '))
item_1_zina_right_format = item_1_zina.quantize(Decimal('1.00'))


# goods 2 section
item_2_title = textwrap.shorten(input('The name of the second product: ').ljust(20, '.'), width=20, placeholder='...')
item_2_quantity = Decimal(input('Enter the desired amount of the second product: '))
item_2_quantity_right_format = item_2_quantity.quantize(Decimal('1.00'))
item_2_zina = Decimal(input('Enter the price of the second product: '))
item_2_zina_right_format = item_2_zina.quantize(Decimal('1.00'))


item_1_total_cost = Decimal(item_1_quantity) * Decimal(item_1_zina)
item_1_total_cost_right_format = item_1_total_cost.quantize(Decimal('1.00'))

item_2_total_cost = Decimal(item_2_quantity) * Decimal(item_2_zina)
item_2_total_cost_right_format = item_2_total_cost.quantize(Decimal('1.00'))

printing_template = '{}\t\t\t\t\t{}\t\t\t\t{}\t\t\t{}'

# printing receipt
print('\n\n\n')
print('фіскальний чек'.capitalize().center(80, '~'))
print('магазин "все для дому"'.upper().center(80))
print('Товар\t\t\t\t\t\t\t\t\tкількість\t\tціна\t\tвартість')
print(printing_template.format(item_1_title, item_1_quantity_right_format, item_1_zina_right_format, item_1_total_cost_right_format))
print(printing_template.format(item_2_title, item_2_quantity_right_format, item_2_zina_right_format, item_2_total_cost_right_format))
print('~' * 80)
print(printing_template.format(
    'ВСЬОГО'.ljust(20),
     (item_1_quantity + item_2_quantity).quantize(Decimal('1.00')),
    'x',
    item_1_total_cost_right_format + item_2_total_cost_right_format
    )
)
print(datetime.now().strftime('%d-%m-%Y %H:%M:%S').rjust(80))
print('\n\n')
