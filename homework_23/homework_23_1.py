import threading


def get_multiplication_result(number):
    print("Multiplication_result is {}".format(number * number))


def get_division_result(number):
    print("Division_result is {}".format(number / number))


def get_sum_result(number):
    print("Sum_result is {}".format(number + number))


thread_1 = threading.Thread(target=get_multiplication_result, args=(5,))
thread_2 = threading.Thread(target=get_division_result, args=(5,))
thread_3 = threading.Thread(target=get_sum_result, args=(5,))
thread_1.start()
thread_2.start()
thread_3.start()
thread_1.join()
thread_2.join()
thread_3.join()

print('Program finished')
