from time import sleep, perf_counter
from threading import Thread


def code_1():
    print('Starting code_1')
    sleep(5)
    print('Running code_1')
    sleep(5)
    print('Finishing code_1')


def code_2():
    print('Starting code_2...')
    sleep(5)
    print('Running code_2...')
    sleep(5)
    print('Finishing code_2...')


def code_3():
    print('Starting code_3...')
    sleep(5)
    print('Running code_3...')
    sleep(5)
    print('Finishing code_3...')


start_time = perf_counter()

thread_1 = Thread(target=code_1)
thread_2 = Thread(target=code_2)
thread_3 = Thread(target=code_3)

thread_1.start()
thread_2.start()
thread_3.start()
thread_1.join()
thread_2.join()
thread_3.join()

end_time = perf_counter()

print("Execution time is {} seconds".format(end_time - start_time))
