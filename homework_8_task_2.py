def calculator(number1, number2, operator):
    """
    Function for simple calculator with four operators
    Args:
        number1: (int)
        number2: (int)
        operator: (str)

    Returns: (int)

    """

    if not isinstance(number1, int):
        print(f'The first argument {number1} is not an integer. Please re-run the function and enter only integer ')
        return
    if not isinstance(number2, int):
        print(f'The second argument {number2} is not an integer. Please re-run the function and enter only integer ')
        return

    if operator == '+':
        return number1 + number2
    elif operator == '-':
        return number1 - number2
    elif operator == '/':
        return number1 / number2
    elif operator == '*':
        return number1 * number2
    else:
        print(f"The third argument {operator} is an invalid operator. Please enter '+' or '-' or '/' or '*' .")
