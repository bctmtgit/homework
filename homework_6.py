import requests
url = 'https://t.ly/_-bm'
response = requests.get(url)
webpage_data = response.json()
products = webpage_data['goods']
total_cost = 0
total_cost_without_gluten = 0
for product in products:
    product_price = product.get('price', 0)
    product_remainder = product.get('remainder', 0)
    total_cost += product_price * product_remainder
    product_cost = product_price * product_remainder
    if not product.get('gluten'):
        total_cost_without_gluten += product_cost
print(f'Total price for all the products is {total_cost}')
print(f'Total price for all the product without gluten is {total_cost_without_gluten}')
