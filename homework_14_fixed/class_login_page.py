from driver_decorator import DriverDecorator
from selenium.webdriver.common.by import By


class LoginPage(DriverDecorator):
    def __init__(self):
        super().__init__()

        self.email = 'admin@yourstore.com'
        self.password = 'admin'

        self.email_locator = "//input[@id='Email']"
        self.password_locator = "//input[@id='Password']"

        self.submit_button = "//button[@type='submit']"

    def clear(self):
        self.driver.find_element(By.XPATH, self.email_locator).clear()
        self.driver.find_element(By.XPATH, self.password_locator).clear()

    def login(self):
        self.driver.find_element(By.XPATH, self.email_locator).send_keys(self.email)
        self.driver.find_element(By.XPATH, self.password_locator).send_keys(self.password)
        self.driver.find_element(By.XPATH, self.submit_button).click()
