from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator
from class_login_page import LoginPage


class TestLoginPage:

    def setup_class(self):
        self.driver = DriverDecorator.get_driver_chrome()

    def setup_method(self):
        self.driver.get("https://admin-demo.nopcommerce.com/")
        self.login_page = LoginPage()

    def test_homework_13(self):
        self.login_page.clear()
        self.login_page.login()
        dashboard = self.driver.find_element(By.XPATH, "//div[@class='content-header']")
        assert dashboard.is_displayed()

    def teardown_method(self):
        pass

    def teardown_class(self):
        self.driver.quit()
