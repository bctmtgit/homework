def define_season(date):
    """
    Function to define a season by user input.
    The format of input is 'xx.yy', where xx - date, yy - month

    Args:
        date: (str)

    Returns: (str)

    """

    months = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
    user_input = date.split('.')
    user_input[1] = int(user_input[1])

    if user_input[1] in months[0]:
        print("Winter time")
    elif user_input[1] in months[1]:
        print("Spring time")
    elif user_input[1] in months[2]:
        print("Summer time")
    elif user_input[1] in months[3]:
        print("Autumn time")
    else:
        print("Incorrect input")
