from class_clear import Allclear
from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator

class ClearEmail:
    def __init__(self):
        driver = DriverDecorator.get_driver_Chrome()
        self.clear_email = Allclear(driver.find_element(By.XPATH, "//input[@id='Email']"))