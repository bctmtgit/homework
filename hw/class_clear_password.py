from class_clear import Allclear
from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator

class ClearPassword:
    def __init__(self):
        driver = DriverDecorator.get_driver_Chrome()
        self.clear_password = Allclear(driver.find_element(By.XPATH, "//input[@id='Password']"))