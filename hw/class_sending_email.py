from class_send import SendAll
from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator
class SendEmail:
    def __init__(self):
        driver = DriverDecorator.get_driver_Chrome()
        self.send_email = SendAll(driver.find_element(By.XPATH, "//button[@type='submit']").send_keys('admin@yourstore.com'))