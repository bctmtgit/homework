from selenium import webdriver
from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator
from class_submit_button import SubmitButton
from class_clear_email import ClearEmail
from class_clear_password import ClearPassword
from class_sending_email import SendEmail
def test_homework_13():


    driver = DriverDecorator.get_driver_Chrome()
    driver.get("https://admin-demo.nopcommerce.com/")
    clearemail = ClearEmail()
    clearemail.clear_email.clear_the_field()
    clearpassword = ClearPassword()
    clearpassword.clear_password.clear_the_field()
    sendemail = SendEmail()
    sendemail.send_email.send_the_data()
    driver.find_element(By.XPATH, "//input[@id='Password']").send_keys('admin')
    submit = SubmitButton()
    submit.submit.button.click_on_the_button()
    dashboard = driver.find_element(By.XPATH, "//div[@class='content-header']")
    assert dashboard.is_displayed()
