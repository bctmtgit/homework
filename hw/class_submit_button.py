from class_button import Allbuttons
from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator

class SubmitButton:
    def __init__(self):
        driver = DriverDecorator.get_driver_Chrome()
        self.submit_button = Allbuttons(driver.find_element(By.XPATH, "//button[@type='submit']"))
