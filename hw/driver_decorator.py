from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver

class DriverDecorator:
    driver = None
    @staticmethod
    def get_driver_Chrome():
        if DriverDecorator.driver is None:
            DriverDecorator.driver = webdriver.Chrome()
            return DriverDecorator.driver
        else:
            return DriverDecorator.driver