class Clear:
    def __init__ (self, element_to_clear):
        self.element = element_to_clear

    def clear(self):
        self.element.clear()