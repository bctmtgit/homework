from driver import Driver
from send import Send
from selenium.webdriver.common.by import By


class SendEmail:
    def __init__(self):
        driver = Driver.get_chrome_driver()
        self.send_email: Send = Send(driver.find_element(By.XPATH, "//input[@id='Email']"))