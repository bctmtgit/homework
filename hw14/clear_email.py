from driver import Driver
from clear import Clear
from selenium.webdriver.common.by import By


class ClearEmail:
    def __init__(self):
        driver = Driver.get_chrome_driver()
        self.clear_email: Clear = Clear(driver.find_element(By.XPATH, "//input[@id='Email']"))