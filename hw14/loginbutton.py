from button import Button
from selenium.webdriver.common.by import By
from driver import Driver

class LoginButton:
    def __init__(self):
        driver = Driver.get_chrome_driver()
        self.login_button: Button = Button(driver.find_element(By.XPATH, "//button[@type='submit']"))
