word = input("Please enter your only one favorite word:")
length = len(word)
favorite_word = "Word {} has {} letters."
if " " in word:
    print("There is no such word. Please re-run the program and don't use space(s)")
elif word == "":
    print("Please re-run the program and enter at least one symbol")
else:
    print(favorite_word.format(word, length))
