def custom_func(element_1, element_2):
    if isinstance(element_1, (int, float)) and isinstance(element_2, (int, float)):
        multiplication_result = element_1 * element_2
        return multiplication_result
    elif isinstance(element_1, str) and isinstance(element_2, str):
        join_result = element_1 + element_2
        return join_result
    else:
        tuple_result = (element_1, element_2)
        return tuple_result
