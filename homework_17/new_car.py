class AddNewCar:
    def __init__(self, car_brand_id, car_model_id, mileage):
        self.carBrandId = car_brand_id
        self.carModelId = car_model_id
        self.mileage = mileage

class EditCar:
    def __init__(self, car_brand_id, car_model_id, mileage):
        self.carBrandId = car_brand_id
        self.carModelId = car_model_id
        self.mileage = mileage

class RemoveCar:
    def __init__(self, car_brand_id, car_model_id, mileage):
        self.carBrandId = car_brand_id
        self.carModelId = car_model_id
        self.mileage = mileage
