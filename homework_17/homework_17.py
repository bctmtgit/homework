import requests
from new_car import AddNewCar, EditCar, RemoveCar
from new_user import RegisterNewUser


class TestAddEditRemoveCar:

    def setup_class(self):
        self.active_session = requests.session()
        register_user = RegisterNewUser('hwtest', 'hwtest', 'hwtest@test.com', 'Qwerty12345', 'Qwerty12345')
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signup', json=register_user.__dict__)


    def test_add_new_car(self):
        add_a_new_car = AddNewCar(1, 1, 3)
        check_addition = self.active_session.post(url='https://qauto2.forstudy.space/api/cars', json=add_a_new_car.__dict__)
        assert check_addition.json()['status'] == 'ok'

    def test_edit_car(self):
        edit_car = EditCar(1, 5, 8)
        check_edit = self.active_session.post(url='https://qauto2.forstudy.space/api/cars/', json=edit_car.__dict__)
        assert check_edit.json()['status'] == 'ok'

    def test_remove_car(self):
        remove_car = RemoveCar(1, 5, 8)
        check_remove = self.active_session.post(url='https://qauto2.forstudy.space/api/cars/', json=remove_car.__dict__)
        assert check_remove.json()['status'] == 'ok'

    def teardown_class(self):
        self.active_session.delete(url="https://qauto2.forstudy.space/api/users")
