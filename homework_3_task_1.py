while True:
    specific_word = input("Please enter any word that contains letter 'o' or 'O':")
    if " " in specific_word:
        print("Please don't use spaces and enter only one word")
    elif 'o' in specific_word or 'O' in specific_word:
        break
