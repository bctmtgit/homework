import re


def validate_str(my_function):
    """This is a decorator that accepts only str value"""

    def wrapper_str(*args, **kwargs):

        arguments = [*args, *kwargs.values()]
        for arg in arguments:
            if not isinstance(arg, str):
                raise ValueError

        return_str = my_function(*args, **kwargs)

        return return_str

    return wrapper_str


@validate_str
def custom_input(user_input: str) -> str:
    """
    Function is designed to take user input, then adjust its length to 25 symbols
    or less and return it back with uppercase letters only if there are any

    If user_input is not a str, raise a ValueError (according to homework_11)

    """

    user_input = user_input[:25]
    user_input = re.sub('[^A-Z]+', '', user_input)

    return user_input
