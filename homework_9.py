import time
import lib


def execution_time(my_function):
    """ This is a decorator that defines a function execution time"""


    def wrapper_time(*args, **kwargs):
        start_point = time.time()
        return_calculator = my_function(*args, **kwargs)
        end_point = time.time()
        print("Execution time of the function is- ", end_point - start_point, 'seconds')
        return return_calculator

    return wrapper_time


@execution_time
def calculator(number1, number2, operator):
    """
    Function for simple calculator with four operators
    Args:
        number1: (int)
        number2: (int)
        operator: (str)

    Returns: (int,float)

    """

    if not isinstance(number1, int):
        print(f'The first argument {number1} is not an integer. Please re-run the function and enter only integer ')
        return
    if not isinstance(number2, int):
        print(f'The second argument {number2} is not an integer. Please re-run the function and enter only integer ')
        return

    if operator == '+':
        return number1 + number2
    elif operator == '-':
        return number1 - number2
    elif operator == '/':
        return number1 / number2
    elif operator == '*':
        return number1 * number2
    else:
        print(f"The third argument {operator} is an invalid operator. Please enter '+' or '-' or '/' or '*' .")
