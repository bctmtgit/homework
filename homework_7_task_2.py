from random import randint


def get_ai_number():
    number = randint(1, 10)
    print(f'AI: {number}')
    return number


def get_user_number(msg):

    while True:
        try:
            return int(input(msg))
        except ValueError:
            print('Number please!')


def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    print(f'Result is: {result}')
    return result


def get_hint(ai_number, user_number):
    difference = abs(user_number - ai_number)
    if difference > 10:
        print(f'Cold! ')

    elif 5 <= difference < 10:
        print(f'Warm! ')
    elif 1 <= difference <= 4:
        print(f'Hot!')


def game_guess_number():
    print('Game begins!')
    tries = get_user_number('Enter how many times you want to try to guess:')
    print('You now have ' + str(tries) + ' attempts')
    ai_number = get_ai_number()

    while tries > 0:
        tries -= 1
        user_number = get_user_number('Enter your number: ')
        is_game_end = check_numbers(ai_number, user_number)

        if is_game_end:
            print('User won')
            return
        else:
            get_hint(ai_number, user_number)
            tries_left = tries
            print(f'{tries_left} attempts left.')


game_guess_number()
