class Figure:
    def __init__(self, name):
        self.name = name


class Triangle(Figure):
    def __init__(self, name, side_1, side_2, side_3=None):
        super(). __init__(name)
        self.side_1 = side_1
        self.side_2 = side_2
        self.side_3 = side_3

    def get_triangle_perimeter(self):
        return self.side_1 + self.side_2 + self.side_3

    def get_triangle_area(self):
        return 0.5 * self.side_1 * self.side_2


class Circle(Figure):
    def __init__(self, name, radius):
        super().__init__(name)
        self.radius = radius

    def get_circle_perimeter(self):
        return 2 * 3.14 * self.radius

    def get_circle_area(self):
        return 3.14 * (self.radius ** 2)


class Rectangle(Figure):
    def __init__(self, name, side_1, side_2):
        super().__init__(name)
        self.side_1 = side_1
        self.side_2 = side_2

    def get_rectangle_perimeter(self):
        return 2 * (self.side_1+self.side_2)

    def get_rectangle_or_square_area(self):
        return self.side_1 * self.side_2


class Square(Rectangle):
    def __init__(self, side):
        super().__init__(side, side, side)
        self.side = side

    def get_square_perimeter(self):
        return 4 * self.side
