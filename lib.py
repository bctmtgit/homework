def define_season(date):
    """
    Function to define a season by user input.
    The format of input is 'xx.yy', where xx - date, yy - month

    Args:
        date: (str)

    Returns: (str)

    """

    months = [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]
    user_input = date.split('.')
    user_input[1] = int(user_input[1])

    if user_input[1] in months[0]:
        print("Winter time")
    elif user_input[1] in months[1]:
        print("Spring time")
    elif user_input[1] in months[2]:
        print("Summer time")
    elif user_input[1] in months[3]:
        print("Autumn time")
    else:
        print("Incorrect input")


def calculator(number1, number2, operator):
    """
    Function for simple calculator with four operators
    Args:
        number1: (int)
        number2: (int)
        operator: (str)

    Returns: (int, float)

    """

    if not isinstance(number1, int):
        print(f'The first argument {number1} is not an integer. Please re-run the function and enter only integer ')
        return
    if not isinstance(number2, int):
        print(f'The second argument {number2} is not an integer. Please re-run the function and enter only integer ')
        return

    if operator == '+':
        return number1 + number2
    elif operator == '-':
        return number1 - number2
    elif operator == '/':
        return number1 / number2
    elif operator == '*':
        return number1 * number2
    else:
        print(f"The third argument {operator} is an invalid operator. Please enter '+' or '-' or '/' or '*' .")
