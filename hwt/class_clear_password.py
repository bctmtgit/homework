from class_clear import Allclear
from selenium.webdriver.common.by import By
from Base_Driver import BaseDriver

class ClearPassword(BaseDriver):
    def __init__(self):
        super().__init__()
        self.clear_password = None
    def get_clear_password(self):
        self.clear_password = Allclear(self.driver.find_element(By.XPATH, "//input[@id='Password']"))
        return self.clear_password