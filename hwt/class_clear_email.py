from class_clear import Allclear
from selenium.webdriver.common.by import By
from Base_Driver import BaseDriver

class ClearEmail(BaseDriver):
    def __init__(self):
        super().__init__()
        self.clear_email = None
    def get_clear_email(self):
        self.clear_email = Allclear(self.driver.find_element(By.XPATH, "//input[@id='Email']"))
        return self.clear_email
