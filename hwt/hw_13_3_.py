from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator
from class_submit_button import SubmitButton
from class_clear_email import ClearEmail
from class_clear_password import ClearPassword


class TestLoginPageTest:

    def setup_class(self):
        self.driver = DriverDecorator.get_driver_Chrome()
        self.clearemail = ClearEmail()
        self.clearpassword = ClearPassword()
        self.submit = SubmitButton()
    def setup_method(self):
        self.driver.get("https://admin-demo.nopcommerce.com/")

    def test_homework_13(self):

        self.clearemail.get_clear_email.clear_the_field()
        self.clearpassword.get_clear_password.clear_the_field()
        self.driver.find_element(By.XPATH, "//input[@id='Password']").send_keys('admin')
        self.submit.get_submit_button.click_on_the_button()
        dashboard = self.driver.find_element(By.XPATH, "//div[@class='content-header']")
        assert dashboard.is_displayed()

    def teardown_method(self):
        pass

    def teardown_class(self):
        self.driver.quit()
