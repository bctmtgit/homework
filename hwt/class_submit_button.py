from class_button import Allbuttons
from selenium.webdriver.common.by import By
from Base_Driver import BaseDriver

class SubmitButton(BaseDriver):
    def __init__(self):
        super().__init__()
        self.submit_button = None
    def get_submit_button(self):
        self.submit_button = Allbuttons(self.driver.find_element(By.XPATH, "//button[@type='submit']"))
        return self.submit_button
