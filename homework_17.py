import requests


class RegisterNewUser:
    def __init__(self, name, last_name, email, password, repeat_password):
        self.name = name
        self.lastName = last_name
        self.email = email
        self.password = password
        self.repeatPassword = repeat_password

class AddNewCar:
    def __init__(self, car_brand_id, car_model_id, mileage):
        self.carBrandId = car_brand_id
        self.carModelId = car_model_id
        self.mileage = mileage

class LoginAction:

    def __init__(self, email, password, remember):
        self.email = email
        self.password = password
        self.remember = remember


class TestAddCar:


    def setup_class(self):
        self.active_session = requests.session()
        register_user = RegisterNewUser('hwtest', 'hwtest', 'hwtest3@test.com', 'Qwerty12345', 'Qwerty12345')
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signup',
                     json=register_user.__dict__)


    def setup_method(self):
        self.active_session.get('https://qauto2.forstudy.space/api/auth/logout')
        login_user = LoginAction('hwtest3@test.com', 'Qwerty12345', False)
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signin', json=login_user.__dict__)


    def test_add_new_car(self):
        add_a_new_car = AddNewCar(2, 3, 2)
        check_addition = self.active_session.post(url='https://qauto2.forstudy.space/api/cars', json=add_a_new_car.__dict__)
        assert check_addition.json()['status'] == 'ok'

    def teardown_class(self):
        self.active_session.delete(url="https://qauto2.forstudy.space/api/users")