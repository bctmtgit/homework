from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import Column, INTEGER, VARCHAR, ForeignKey

Base = declarative_base()


class RoleModel(Base):
    __tablename__ = "roles"
    role_id = Column(INTEGER, primary_key=True)
    title = Column(VARCHAR(50))
    users = relationship('UserModel', back_populates='role')

    def __str__(self):
        return f'id = {self.role_id}, title = {self.title}'


class UserModel(Base):
    __tablename__ = 'users'
    user_id = Column(INTEGER, primary_key=True)
    email = Column(VARCHAR(50))
    password = Column(VARCHAR(50))
    role_id = Column(ForeignKey("roles.role_id"))
    role = relationship('RoleModel', back_populates='users')

    def __str__(self):
        return f'id = {self.user_id}, email = {self.email}, password = {self.password},role_id = {self.role_id}'
