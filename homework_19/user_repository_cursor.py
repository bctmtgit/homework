import psycopg2


class BaseRepository:
    def __init__(self):
        self.connection = psycopg2.connect(
            user='hwtest',
            password='hwtest',
            host='127.0.0.1',
            port='5432',
            database='postgres'
        )
        self.connection.set_session(autocommit=True)
        self.cursor = self.connection.cursor()


class UserRepositoryCursor(BaseRepository):
    def __init__(self):
        super().__init__()

    def get_all_users(self):
        self.cursor.execute('SELECT * FROM users;')
        return self.cursor.fetchall()

    def get_user_by_email(self, email):
        self.cursor.execute(f"SELECT * FROM users WHERE users.email = '{email}';")
        return self.cursor.fetchone()

    def insert_user(self, user_id, email, password, role_id):
        self.cursor.execute(f"INSERT INTO users (user_id, email, password, role_id) VALUES ('{user_id}', '{email}', '{password}', '{role_id}');")

    def delete_by_id(self, user_id):
        self.cursor.execute(f"DELETE FROM users WHERE user_id = {user_id};")

    def update_user_by_email(self, old_email, new_email):
        self.cursor.execute(f"UPDATE users SET email = '{new_email}' WHERE users.email = '{old_email}';")
