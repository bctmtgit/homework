from alchemy.models.user_model import UserModel
from alchemy.session_sql import session


class UserRepository:
    def __init__(self):
        self.session = session
        self.model = UserModel

    def get_all_users(self):
        return self.session.query(self.model).all()

    def get_user_by_id(self, user_id):
        user = self.session.get(self.model, {'user_id': user_id})
        return user

    def add_user(self, user):
        self.session.add(user)

    def remove_user_by_id(self, user_id):
        user = self.session.get(self.model, {'user_id': user_id})
        self.session.delete(user)
