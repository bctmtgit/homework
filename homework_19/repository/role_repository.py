from alchemy.models.role_model import RoleModel
from alchemy.session_sql import session


class RoleRepository:
    def __init__(self):
        self.session = session
        self.model = RoleModel

    def get_all_roles(self):
        return self.session.query(self.model).all()

    def get_role_by_id(self, role_id):
        role = self.session.get(self.model, {'role_id': role_id})
        return role

    def add_role(self, role):
        self.session.add(role)

    def remove_role_by_id(self, role_id):
        role = self.session.get(self.model, {'role_id': role_id})
        self.session.delete(role)
