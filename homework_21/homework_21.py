from pymongo import MongoClient
import json


mongo_client = MongoClient('mongodb://localhost:27017')
db = mongo_client['hwtest']
collection = db['my_collect']

with open('new_data.json') as file:
    json_data = json.load(file)
collection.insert_many(json_data)


class QA:
    def __init__(self, manual: str, automation: str):
        self.manual: str = manual
        self.automation: str = automation


class Skills:
    def __init__(self, language: str, pc_language: str, qa: QA):
        self.language: str = language
        self.pc_language: str = pc_language
        self.qa: QA = qa

    def __repr__(self):
        return f'{self.language}, {self.pc_language}, {self.qa}'


class User:
    def __init__(self, _id: str, name: str, email: str, skills: list[Skills]):
        self._id: id = _id
        self.name: str = name
        self.email: str = email
        self.skills: list[Skills] = skills

    def __str__(self):
        return f'{self._id} is an id, {self.name} is a name, {self.email} is an email'


def convert_user_to_object(base_dict: dict) -> User:
    list_of_skills = base_dict["skills"]
    skills_list = list()
    for skill in list_of_skills:
        qa_of_skill = QA(skill['qa']['manual'], skill['qa']['automation'])
        skills_in_list = Skills(skill['language'], skill['pc_language'], qa_of_skill)
        skills_list.append(skills_in_list)
        result_user = User(base_dict["_id"], base_dict["name"], base_dict["email"], skills_list)
        return result_user
