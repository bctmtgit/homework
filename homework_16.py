import requests


class LoginAction:

    def __init__(self, email, password, remember):
        self.email = email
        self.password = password
        self.remember = remember


class RegisterAction:

    def __init__(self, name, last_name, email, password, repeat_password):

        self.name = name
        self.lastName = last_name
        self.email = email
        self.password = password
        self.repeatPassword = repeat_password


class TestRegLogDel:

    def setup_class(self):
        self.active_session = requests.session()
        self.active_session.get('https://qauto2.forstudy.space/api/auth/logout')


    def test_register(self):
        register_action = RegisterAction('HWTest', 'TestHW', 'hwtest@test.com', 'Qwerty12345', 'Qwerty12345')
        result_signup = self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signup', json=register_action.__dict__)
        assert result_signup.json()['status'] == 'ok'


    def test_login_action(self):
        login_action = LoginAction('hwtest@test.com', 'Qwerty12345', False)
        result_signin = self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signin', json=login_action.__dict__)
        assert result_signin.json()['status'] == 'ok'


    def test_delete_action(self):
        result_deletion = self.active_session.delete(url='https://qauto2.forstudy.space/api/users')
        assert result_deletion.json()['status'] == 'ok'


    def teardown_class(self):
        self.active_session.get("https://qauto2.forstudy.space/api/auth/logout")
        login_action = LoginAction('hwtest@test.com', 'Qwerty12345', False)
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signin', json=login_action.__dict__)
        self.active_session.delete(url='https://qauto2.forstudy.space/api/users')
