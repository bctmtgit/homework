from driver_decorator import DriverDecorator


class BaseDriver:
    def __init__(self):
        self.driver = DriverDecorator.get_driver_chrome()
