from class_send_email import ClassSendEmail
from selenium.webdriver.common.by import By
from base_driver import BaseDriver


class EmailLocator(BaseDriver):
    def __init__(self):
        super().__init__()
        self.email_locator = ClassSendEmail(self.driver.find_element(By.XPATH, "//input[@id='Email']"))
