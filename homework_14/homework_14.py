from selenium.webdriver.common.by import By
from driver_decorator import DriverDecorator
from class_submit_button import SubmitButton
from class_clear_email import ClearEmail
from class_clear_password import ClearPassword
from class_email_locator import EmailLocator
from class_password_locator import PasswordLocator


class TestLoginPage:

    def setup_class(self):
        self.driver = DriverDecorator.get_driver_chrome()

    def setup_method(self):
        self.driver.get("https://admin-demo.nopcommerce.com/")

    def test_homework_13(self):
        self.clear_email_field = ClearEmail()
        self.clear_email_field.clear_email.clear_the_field()
        self.clear_password_field = ClearPassword()
        self.clear_password_field.clear_password.clear_the_field()
        self.email_field = EmailLocator()
        self.email_field.email_locator.send_info_email()
        self.password_field = PasswordLocator()
        self.password_field.password_locator.send_info_pass()
        self.submit = SubmitButton()
        self.submit.submit_button.click_on_the_button()
        dashboard = self.driver.find_element(By.XPATH, "//div[@class='content-header']")
        assert dashboard.is_displayed()

    def teardown_method(self):
        pass

    def teardown_class(self):
        self.driver.quit()
