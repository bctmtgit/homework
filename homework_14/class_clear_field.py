class AllClear:
    def __init__(self, clear_element):
        self.element = clear_element

    def clear_the_field(self):
        self.element.clear()
