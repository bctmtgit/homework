from class_send_password import ClassSendPassword
from selenium.webdriver.common.by import By
from base_driver import BaseDriver


class PasswordLocator(BaseDriver):
    def __init__(self):
        super().__init__()
        self.password_locator = ClassSendPassword(self.driver.find_element(By.XPATH, "//input[@id='Password']"))
