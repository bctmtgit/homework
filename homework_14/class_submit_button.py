from class_button import AllButtons
from selenium.webdriver.common.by import By
from base_driver import BaseDriver


class SubmitButton(BaseDriver):
    def __init__(self):
        super().__init__()
        self.submit_button = AllButtons(self.driver.find_element(By.XPATH, "//button[@type='submit']"))
