class AllButtons:
    def __init__(self, button_element):
        self.element = button_element

    def click_on_the_button(self):
        self.element.click()

    def if_button_enabled(self):
        self.element.is_enabled()
