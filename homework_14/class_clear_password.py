from class_clear_field import AllClear
from selenium.webdriver.common.by import By
from base_driver import BaseDriver


class ClearPassword(BaseDriver):
    def __init__(self):
        super().__init__()
        self.clear_password = AllClear(self.driver.find_element(By.XPATH, "//input[@id='Password']"))
