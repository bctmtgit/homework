from class_clear_field import AllClear
from selenium.webdriver.common.by import By
from base_driver import BaseDriver


class ClearEmail(BaseDriver):
    def __init__(self):
        super().__init__()
        self.clear_email = AllClear(self.driver.find_element(By.XPATH, "//input[@id='Email']"))
