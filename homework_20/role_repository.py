from pony.orm import db_session, select
from models import Role


class RoleRepository:
    def __init__(self):
        self.model = Role

    @db_session
    def get_all_roles(self):
        roles = select(role for role in self.model).page(1).to_list()
        return roles

    @db_session
    def get_all_by_lambda(self):
        roles = Role.select(lambda role: role)
        return roles.page(1).to_list()

    @db_session
    def get_role_by_title_by_lambda(self, title):
        roles = Role.select(lambda role: role.title == title)
        return roles.page(1).to_list()
