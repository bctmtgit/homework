from pony.orm import Database, PrimaryKey, Required, Set
db = Database()
db.bind(provider='postgres', user='hwtest', password='hwtest', host='127.0.0.1', database='postgres')


class Role(db.Entity):
    _table_ = 'roles'
    role_id = PrimaryKey(int, auto=True)
    title = Required(str, 50)
    users = Set('User')

    def __str__(self):
        return f'id = {self.role_id}; title = {self.title}'

    def __repr__(self):
        return f'id = {self.role_id}; title = {self.title}'


class User(db.Entity):
    _table_ = 'users'
    user_id = PrimaryKey(int, auto=True)
    email = Required(str, 50)
    password = Required(str, 50)
    role = Required(Role, column='role_id')

    def __str__(self):
        return f'id = {self.user_id}; email = {self.email}'

    def __repr__(self):
        return f'id = {self.user_id}; email = {self.email}'


db.generate_mapping(create_tables=False)
