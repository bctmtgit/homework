from pony.orm import db_session, select
from models import User


class UserRepository:
    def __init__(self):
        self.model = User

    @db_session
    def get_all_users_by_lambda(self):
        users = User.select(lambda user: user)
        return users.page(1).to_list()

    @db_session
    def get_all_users(self):
        users = select(user for user in self.model).page(1).to_list()
        return users

    @db_session
    def get_user_by_id_by_lambda(self, user_id):
        users = User.select(lambda user: user.user_id == user_id)
        return users.page(1).to_list()
