age = input("Please enter your age and use only integers:")
if age.isdigit():
    age = int(age)
    if age < 7:
        print("Where are your parents?")
    elif 7 < age < 16:
        print("It's a movie for adults only!")
    elif age > 65:
        print("Please show us your pension card!")
    elif '7' in str(age):
        print("You will be lucky today!")
    else:
        print("There are no tickets for now!")
else:
    print("Please re-run the program and use only integers")
