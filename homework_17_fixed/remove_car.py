from driver_decorator import DriverDecorator
from selenium.webdriver.common.by import By


class RemoveCar(DriverDecorator):
    def __init__(self):
        super().__init__()
        self.edit_car_locator = "//span[@class='icon icon-edit']"
        self.remove_car_locator = "//button[@class='btn btn-outline-danger']"
        self.remove_car_confirmation_locator = "//button[@class='btn btn-danger']"


    def click_edit_car_locator(self):
        self.driver.find_element(By.XPATH, self.edit_car_locator).click()

    def click_remove_car_locator(self):
        self.driver.find_element(By.XPATH, self.remove_car_locator).click()

    def click_remove_car_confirmation_locator(self):
        self.driver.find_element(By.XPATH, self.remove_car_confirmation_locator).click()
