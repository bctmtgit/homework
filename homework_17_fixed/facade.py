from login_page import LoginPage
from remove_car import RemoveCar
import allure


class Facade:
    def __init__(self):
        self.login_page = LoginPage()
        self.remove_car = RemoveCar()

    @allure.step('login to webpage')
    def login(self):
        self.login_page.click_sign_in_button()
        self.login_page.enter_email()
        self.login_page.enter_password()
        self.login_page.click_login_button()

    @allure.step('remove car from profile')
    def remove_new_car(self):
        self.remove_car.click_edit_car_locator()
        self.remove_car.click_remove_car_locator()
        self.remove_car.click_remove_car_confirmation_locator()
