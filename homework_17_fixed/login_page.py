from driver_decorator import DriverDecorator
from selenium.webdriver.common.by import By


class LoginPage(DriverDecorator):
    def __init__(self):
        super().__init__()

        self.email = 'hw_test@test.com'
        self.password = 'Qwerty12345'

        self.sign_in_button_locator = "//button[text()='Sign In']"
        self.email_field_locator = "//input[@id='signinEmail']"
        self.password_field_locator = "//input[@id='signinPassword']"
        self.login_button_locator = "//button[@class='btn btn-primary']"

    def click_sign_in_button(self):
        self.driver.find_element(By.XPATH, self.sign_in_button_locator).click()

    def enter_email(self):
        self.driver.find_element(By.XPATH, self.email_field_locator).click()
        self.driver.find_element(By.XPATH, self.email_field_locator).send_keys(self.email)

    def enter_password(self):
        self.driver.find_element(By.XPATH, self.password_field_locator).click()
        self.driver.find_element(By.XPATH, self.password_field_locator).send_keys(self.password)

    def click_login_button(self):
        self.driver.find_element(By.XPATH, self.login_button_locator).click()
