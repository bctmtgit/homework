import requests
from add_car import AddNewCar
from new_user import RegisterNewUser
from driver_decorator import DriverDecorator
from profile_page import RemoveCar
from login_page import LoginPage
from selenium.webdriver.common.by import By


class TestAddCar:

    def setup_class(self):
        self.active_session = requests.session()
        self.driver = DriverDecorator.get_driver_Chrome()
        self.driver.implicitly_wait(1)
        register_user = RegisterNewUser('hwtest', 'hwtest', 'hw_test@test.com', 'Qwerty12345', 'Qwerty12345')
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signup', json=register_user.__dict__)
        self.remove_car = RemoveCar()
        self.login_page = LoginPage()


    def test_add_new_car(self):
        add_a_new_car = AddNewCar(1, 1, 3)
        check_addition = self.active_session.post(url='https://qauto2.forstudy.space/api/cars', json=add_a_new_car.__dict__)
        assert check_addition.json()['status'] == 'ok'


    def test_remove_car(self):
        self.driver.get("https://guest:welcome2qauto@qauto2.forstudy.space/")
        self.login_page.click_sign_in_button()
        self.login_page.enter_email()
        self.login_page.enter_password()
        self.login_page.click_login_button()
        self.remove_car.click_edit_car_locator()
        self.remove_car.click_remove_car_locator()
        self.remove_car.click_remove_car_confirmation_locator()
        confirm_removal = self.driver.find_element(By.XPATH, "//p[text()='You don’t have any cars in your garage']")
        assert confirm_removal.is_displayed()


    def teardown_class(self):
        self.active_session.delete(url="https://qauto2.forstudy.space/api/users")
