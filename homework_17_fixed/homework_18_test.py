import allure
import requests
from add_car import AddNewCar
from register_new_user import RegisterNewUser
from driver_decorator import DriverDecorator
from selenium.webdriver.common.by import By
import time
from facade import Facade


class TestAddCar:
    def setup_class(self):
        self.active_session = requests.session()
        self.driver = DriverDecorator.get_driver_Chrome()
        self.driver.implicitly_wait(1)
        register_new_user = RegisterNewUser('hwtest', 'hwtest', 'hw_test@test.com', 'Qwerty12345', 'Qwerty12345')
        self.active_session.post(url='https://qauto2.forstudy.space/api/auth/signup', json=register_new_user.__dict__)
        self.facade = Facade()

    @allure.description('Adding a new car via API')
    def test_add_new_car(self):
        add_a_new_car = AddNewCar(1, 1, 3)
        check_addition = self.active_session.post(url='https://qauto2.forstudy.space/api/cars', json=add_a_new_car.__dict__)
        assert check_addition.json()['status'] == 'ok'

    @allure.description('Removing a car using User Interface')
    def test_remove_car(self):
        self.driver.get("https://guest:welcome2qauto@qauto2.forstudy.space/")
        self.facade.login()
        self.facade.remove_new_car()
        confirm_removal = self.driver.find_element(By.XPATH, "//p[text()='You don’t have any cars in your garage']")
        assert confirm_removal.is_displayed()

    def teardown_method(self):
        screenshot_name_with_current_time = time.strftime('%Y%m%d-%H%M%S')
        allure.attach(self.driver.get_screenshot_as_png(), name=screenshot_name_with_current_time)

    def teardown_class(self):
        self.active_session.delete(url="https://qauto2.forstudy.space/api/users")
