from selenium import webdriver
from selenium.webdriver.common.by import By
from driver import Driver
from loginbutton import LoginButton
from send_email import SendEmail
from send_password import SendPassword
from clear_email import ClearEmail
from clear_pass import ClearPassword


def test_homework_13():


    driver = Driver.get_chrome_driver()
    driver.get("https://admin-demo.nopcommerce.com/")
    clear_e = ClearEmail()
    clear_e.clear_email.clear()
    send_e = SendEmail()
    send_e.send_email.send_keys('admin@yourstore.com')
    clear_p = ClearPassword()
    clear_p.clear_password.clear()
    send_p = SendPassword()
    send_p.send_password.send_keys('admin')
    login_b = LoginButton()
    login_b.login_button.click()
    dashboard = driver.find_element(By.XPATH, "//div[@class='content-header']")
    assert dashboard.is_displayed()
