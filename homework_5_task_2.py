import requests
url = 'https://dummyjson.com/users?limit=100'
response = requests.get(url)
webpage_data = response.json()
users = webpage_data['users']
for person in users:
    if person.get('address', "").get('city', "").lower() == 'louisville':
        first_name = person.get("firstName")
        last_name = person.get("lastName")
        user = first_name + " " + last_name
        print(user + ' lives in Louisville.')
