import asyncio


def zero_print(my_function):

    async def wrapper_print():
        print('zero')
        print_zero = my_function()
        await print_zero

    return wrapper_print


@zero_print
async def function_1():
    print('one')
    await function_2()
    print('four')
    print('five')


async def function_2():
    print('two')
    print('three')


asyncio.run(function_1())
