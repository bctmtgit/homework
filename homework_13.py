from selenium import webdriver
from selenium.webdriver.common.by import By


def test_homework_13():


    driver = webdriver.Chrome()
    driver.get("https://admin-demo.nopcommerce.com/")
    driver.find_element(By.XPATH, "//input[@id='Email']").clear()
    driver.find_element(By.XPATH, "//input[@id='Email']").send_keys('admin@yourstore.com')
    driver.find_element(By.XPATH, "//input[@id='Password']").clear()
    driver.find_element(By.XPATH, "//input[@id='Password']").send_keys('admin')
    driver.find_element(By.XPATH, "//button[@type='submit']").click()
    dashboard = driver.find_element(By.XPATH, "//div[@class='content-header']")
    assert dashboard.is_displayed()
