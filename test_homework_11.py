import homework_11
import pytest


testcases_custom_input_positive = [
    ('Ascfhd!-./345321sdcvfgrpA', 'AA'),  # Enter 25 symbols with one uppercase letter at the beginning and at the end
    ('ascfhd!-./3R45321sdcvfgrb', 'R'),  # Enter 24 symbols with one uppercase letter in the middle
    ('AXCH!-askrAKDLOEXqAS1FG-/', 'AXCHAKDLOEXASFG'),  # Enter 15 uppercase letters within first 25 symbols

]

testcases_custom_input_negative = [
    ('1scfhd!-./345d321sdcvfgraAXCH!-askrAKDLOEXqAS1FG-/', ''),  # Enter 15 uppercase letters outside of first 25 symbols
    ('Ascfhd!-./345D321sdcvfgrAG', 'ADA'),  # Enter 26 symbols with three uppercase letters within first 25 symbols
    ('efposfopsjgisg', ''),  # Enter only_lowercase letters
    ('2425762842523495', ''),  # Enter only numbers
    ('!-//////@".<,,.#$#^*&!!]', ''),  # Enter only special characters
]

testcases_custom_input_invalid_input_negative = [
    (1321, ValueError),  # Enter int as a user_input
    (0.5, ValueError),   # Enter float as a user_input
    (True, ValueError),  # Enter bool as a user_input
    (False, ValueError),  # Enter bool as a user_input
]


@pytest.mark.parametrize('user_input, result', testcases_custom_input_positive)
def test_custom_input_positive(user_input, result):
    assert homework_11.custom_input(user_input) == result


@pytest.mark.parametrize('user_input, result', testcases_custom_input_negative)
def test_custom_input_negative(user_input, result):
    assert homework_11.custom_input(user_input) == result


@pytest.mark.parametrize('user_input, error', testcases_custom_input_invalid_input_negative)
def test_custom_input_invalid_input_negative(user_input, error):
    with pytest.raises(error):
        homework_11.custom_input(user_input)
