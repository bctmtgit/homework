lower_limit = 35.9
upper_limit = 37.339
shops_and_prices = {
    "citos": 47.999,
    "BB_studio": 42.999,
    "mo-no": 49.999,
    "my-main-service": 37.245,
    "buy-now": 38.324,
    "x-store": 37.166,
    "the-partner": 38.988,
    "store-123": 37.720,
    "roze-tka": 38.003
}
for shop, price in shops_and_prices.items():
    if lower_limit <= price <= upper_limit:
        print("The shop " + shop + " matches our search criteria.")
